//Метод forEach обращается к каждому элементу массива по очереди, итерируется до конца массива.

let arr = ["hello", "world", 23, "23", null, undefined];
let type = "string";

function filterByType(arr, type) {
	let result = arr.filter((element) => typeof element !== type);
	return result;
}
const result = filterByType(arr, type);
console.log(result);
